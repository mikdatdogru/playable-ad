import React, { Component } from 'react';
import PropTypes from 'prop-types';
import NumPreview from '../components/NumPreview';
import Numpad from '../components/Numpad';
import logoImg from '../assets/logo.jpg';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number: '',
    };
  }

  openUrl = () => {
    // FbPlayableAd.onCTAClick(); // for facebook
    window.open('https://itunes.apple.com/us/app/space-journey/id1439556177');
  };

  addNumber = number => {
    if (this.state.number.length <= 10) {
      this.setState(prevState => ({
        number: `${prevState.number}${number}`,
      }));
    }
  };

  removeNumber = () => {
    if (this.state.number.length > 0) {
      this.setState(prevState => ({
        number: prevState.number.slice(0, -1),
      }));
    }
  };

  render() {
    return (
      <div className="container-fluid mobileAds">
        <div className="imgContainer">
          <img src={logoImg} className="img-fluid" alt="number finder" />
        </div>
        <p className="slogan">İstediğin telefon numarasını tuşla ve kimin olduğunu öğren</p>
        <NumPreview numbers={this.state.number} removeNumber={this.removeNumber} />
        <Numpad addNumber={this.addNumber} />

        <div className="row m-t-5 m-b-5">
          <div className="offset-1 col-10">
            <button
              type="button"
              className="btn btn-primary btn-lg btn-block actionButton"
              onClick={this.openUrl}
            >
              SORGULA
            </button>
          </div>
        </div>
      </div>
    );
  }
}

Home.propTypes = {};
Home.defaultProps = {};

export default Home;
