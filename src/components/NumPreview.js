import React from 'react';
import PropTypes from 'prop-types';
import deleteImg from '../assets/delete.png';

const NumPreview = props => {
  let num = props.numbers;
  if (num.length > 1) {
    num = `${num.substr(0, 1)}(${num.substr(1, 3)}${num.length < 4 ? ' ' : ''}) ${num.substr(
      4,
      3,
    )} ${num.substr(7, 2)} ${num.substr(9)}`;
  }

  return (
    <div className="NumPreview">
      {num}

      {props.numbers.length > 0 && (
        <img
          onTouchStart={props.removeNumber}
          className="removeButton"
          src={deleteImg}
          alt="delete"
        />
      )}
    </div>
  );
};

NumPreview.propTypes = {};
NumPreview.defaultProps = {};

export default NumPreview;
