import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
// import ReactRouterPropTypes from 'react-router-prop-types';

class Num extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
    };
  }

  onTouch = action => () => {
    this.setState({
      active: action,
    });

    if (action) {
      this.props.addNumber(this.props.number);
    }
  };

  render() {
    return (
      <div
        className={classnames('nums', { active: this.state.active })}
        onTouchStart={this.onTouch(true)}
        onTouchEnd={this.onTouch(false)}
      >
        <h1>{this.props.number}</h1>
        {!this.props.centered && (
          <h6 className={classnames({ 'opacity-0': this.props.words === 'null' })}>
            {this.props.words}
          </h6>
        )}
      </div>
    );
  }
}

Num.propTypes = {};
Num.defaultProps = {};

export default Num;
