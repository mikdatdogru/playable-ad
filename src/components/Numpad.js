import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Num from './Num';

class Numpad extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="Numpad">
        <div className="numWrap">
          <Num addNumber={number => this.props.addNumber(number)} number={1} />
        </div>
        <div className="numWrap">
          <Num addNumber={number => this.props.addNumber(number)} number={2} words="ABC" />
        </div>
        <div className="numWrap">
          <Num addNumber={number => this.props.addNumber(number)} number={3} words="DEF" />
        </div>
        <div className="numWrap">
          <Num addNumber={number => this.props.addNumber(number)} number={4} words="GHI" />
        </div>
        <div className="numWrap">
          <Num addNumber={number => this.props.addNumber(number)} number={5} words="JKL" />
        </div>
        <div className="numWrap">
          <Num addNumber={number => this.props.addNumber(number)} number={6} words="MNO" />
        </div>
        <div className="numWrap">
          <Num addNumber={number => this.props.addNumber(number)} number={7} words="PQRS" />
        </div>
        <div className="numWrap">
          <Num addNumber={number => this.props.addNumber(number)} number={8} words="TUV" />
        </div>
        <div className="numWrap">
          <Num addNumber={number => this.props.addNumber(number)} number={9} words="WXYZ" />
        </div>
        <div className="numWrap">
          <Num addNumber={number => this.props.addNumber(number)} number="*" centered />
        </div>
        <div className="numWrap">
          <Num addNumber={number => this.props.addNumber(number)} number={0} words="+" />
        </div>
        <div className="numWrap">
          <Num addNumber={number => this.props.addNumber(number)} number="#" centered />
        </div>
      </div>
    );
  }
}

Numpad.propTypes = {};
Numpad.defaultProps = {};

export default Numpad;
