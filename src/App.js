import React, { Fragment } from 'react';
import Home from './pages/Home';

const App = props => {
  return (
    <Fragment>
      <Home />
    </Fragment>
  );
};

App.propTypes = {};
App.defaultProps = {};

export default App;
