import webpack from 'webpack';
import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';

module.exports = {
  devtool: false,
  context: path.resolve(__dirname, 'src'),
  entry: path.join(__dirname, 'src', 'index.js'),
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'index.bundle.js',
  },
  mode: process.env.NODE_ENV || 'development',
  resolve: {
    extensions: ['.jsx', '.js', '.json', '.scss'],
    modules: [path.resolve(__dirname, 'src'), 'node_modules'],
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
          },
        ],
      },
      {
        test: /\.(css|scss)$/,
        use: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
          },
          {
            loader: 'sass-loader',
          },
        ],
      },
      {
        test: /\.json$/,
        use: [{ loader: 'json' }],
      },
      {
        test: /\.(xml|html|txt|md)$/,
        use: [{ loader: 'raw-loader' }],
      },
      {
        test: /\.(svg|woff2?|ttf|eot)(\?.*)?$/i,
        use: [
          {
            loader:
              process.env.NODE_ENV === 'production'
                ? 'file?name=[path][name]_[hash:base64:5].[ext]'
                : 'url',
          },
        ],
      },
      {
        test: /\.(svg|png|jp?g|gif)$/i,
        use: [
          {
            loader: 'url-loader',
          },
        ],
      },
    ],
  },
  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    new HtmlWebpackPlugin({
      template: path.join(__dirname, 'public', 'index.html'),
    }),
  ],
  stats: { colors: true },
  node: {
    global: true,
    process: false,
    Buffer: false,
    __filename: false,
    __dirname: false,
    setImmediate: false,
  },
  devServer: {
    contentBase: path.join(__dirname, 'src'),
    port: process.env.PORT || 8080,
    host: 'localhost',
    // publicPath: "/build",
    // historyApiFallback: true,
    hot: true,
    open: true,
    inline: false,
  },
};
